# Exposure to phenols during pregnancy and the first year of life in a new type of couple-child cohort relying on repeated urine biospecimens

Git repo for (Rolland et al., 2020)

## Analysis overview

This analysis was performed using [drake](https://github.com/ropensci/drake) workflow management package. 

To run the analysis only the `makefile.R` script needs to be executed. This makefile calls the drake analysis plan (`R/phenols_descriptive_plan.R`) which outlines the successive analysis steps and calls the different functions used to execute different analysis steps, to be found in the `R/` folder. 

Re-running the analysis also requires an additional `data/` folder not shared here, containing the different data sets presented hereafter. These data can only be provided upon request and approval by the SEPAGES stearing comity.

### data/ folder

Analysis input data-files are not made available as they contain sensitive information. The analysis input data files are:

* `limits_02_09-19.csv` = LOD and LOQ for the different phenols
* `crb_11-10-19.csv` = data from Grenoble's Biological resource center (Centre de Ressources Biologiques (CRB)), sample specific info such as sampling date, specific gravity, etc 
* `samples_02_09_19.csv` = phenol urine samples from the Norwegian Institute of Public Health (NIPH), containing phenol concentrations for each individual at each sampling time points
* `bdd_grossesse_v3.dta` = Sepages data containing individual pregnancy related info (mom age, BMI, smoking etc)
* `demande_181205.dta` = Extra Sepages data containing post-natal related info (child height, weight, etc)
* `jobs_to_jobs_cat_3.xlsx` = Manual categorisation of Sepages occupation info
* `t3_new_jobs.csv` = Manual categorisation of Sepages occupation info for mothers who changed job between measure at second and measure at third trimester of pregnancy
* `Tobacco_cor.dta` = Smoking status during pregnancy for each woman
* `fig_1_data.csv` = manually produced data for figure 1 (comparison with other cohorts)

### results/ folder

Results files in the `results/` folder:

* `main_results_date.html` = article main results
* `sensitivity_analyses_date.html` = article sensitivity analyses
* `supplementary_material_date.html` = article supplementary material

Code used to produce these files is found in the corresponding .Rmd files.

### R/ folder

This folder contains all the functions used for the analysis, called in the `makefile.R` script.

* `phenols_descriptive_plan.R` = drake plan outlying the different steps of the analysis and setting the dependencies
* `data_preparation.R` = all functions relative to data preparation except occupation and breastfeeding (each very long and seperated)
* `figures.R` = code for all paper figures
* `helpers.R` = miscelaneous helper functions
* `lists.R` = miscelaneous helper lists of compounds, variables, etc
* `milk_data_preparation.R` = breastfeeding data preparation
* `occupation_data_preparation.R` = maternal occupation data preparation
* `packages.R` = analysis packages
* `stat_models.R` = analysis statistical models
* `table_preparation.R` = table preparation, formatting, etc

### packages

All versions of the packages used are saved in the `packrat/` folder at the version used to produce these results.

## Article info

[Link to article](https://www.sciencedirect.com/science/article/pii/S0160412019340681?via%3Dihub)

## Session info

```
R version 3.6.3 (2020-02-29)
Platform: x86_64-w64-mingw32/x64 (64-bit)
Running under: Windows 10 x64 (build 18363)

Matrix products: default

locale:
[1] LC_COLLATE=French_France.1252  LC_CTYPE=French_France.1252    LC_MONETARY=French_France.1252
[4] LC_NUMERIC=C                   LC_TIME=French_France.1252    

attached base packages:
[1] stats     graphics  grDevices utils     datasets  methods   base     

other attached packages:
 [1] stringdist_0.9.5.5 xfun_0.12          colorspace_1.4-1   ICC_2.3.0          resample_0.4      
 [6] see_0.4.1          here_0.1           lme4_1.1-21        Matrix_1.2-18      storr_1.2.1       
[11] mice_3.8.0         EnvStats_2.3.1     broom_0.5.5        MASS_7.3-51.5      labelled_2.2.2    
[16] NADA_1.6-1         survival_3.1-8     haven_2.2.0        kableExtra_1.1.0   xlsx_0.6.3        
[21] lubridate_1.7.4    forcats_0.5.0      stringr_1.4.0      dplyr_0.8.5        purrr_0.3.3       
[26] readr_1.3.1        tidyr_1.0.2        tibble_2.1.3       ggplot2_3.3.0      tidyverse_1.3.0   
[31] tidyselect_1.0.0   drake_7.11.0      

loaded via a namespace (and not attached):
 [1] nlme_3.1-144      fs_1.3.2          insight_0.8.2     filelock_1.0.2    webshot_0.5.2    
 [6] progress_1.2.2    httr_1.4.1        rprojroot_1.3-2   tools_3.6.3       backports_1.1.5  
[11] utf8_1.1.4        R6_2.4.1          DBI_1.1.0         withr_2.1.2       prettyunits_1.1.1
[16] compiler_3.6.3    cli_2.0.2         rvest_0.3.5       xml2_1.2.5        labeling_0.3     
[21] bookdown_0.18     bayestestR_0.5.3  scales_1.1.0      ggridges_0.5.2    digest_0.6.25    
[26] txtq_0.2.0        minqa_1.2.4       rmarkdown_2.1     pkgconfig_2.0.3   htmltools_0.4.0  
[31] highr_0.8         dbplyr_1.4.2      htmlwidgets_1.5.1 rlang_0.4.5       readxl_1.3.1     
[36] rstudioapi_0.11   farver_2.0.3      visNetwork_2.0.9  generics_0.0.2    jsonlite_1.6.1   
[41] magrittr_1.5      parameters_0.6.0  Rcpp_1.0.3        munsell_0.5.0     fansi_0.4.1      
[46] lifecycle_0.2.0   stringi_1.4.6     yaml_2.2.1        plyr_1.8.6        grid_3.6.3       
[51] parallel_3.6.3    crayon_1.3.4      lattice_0.20-38   splines_3.6.3     xlsxjars_0.6.1   
[56] hms_0.5.3         knitr_1.28        pillar_1.4.3      igraph_1.2.4.2    boot_1.3-24      
[61] base64url_1.4     effectsize_0.2.0  reprex_0.3.0      glue_1.3.1        packrat_0.5.0    
[66] evaluate_0.14     modelr_0.1.6      vctrs_0.2.4       rmdformats_0.3.7  nloptr_1.2.2.1   
[71] cellranger_1.1.0  gtable_0.3.0      assertthat_0.2.1  viridisLite_0.3.0 rJava_0.9-11     
[76] ellipsis_0.3.0   
 

```


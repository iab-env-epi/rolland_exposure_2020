# Preparation of the mom_occupation variable
# M. Rolland
# 05/09/19

# occupation data preparation: cashier, healthcare worker, other
prepare_mom_occupation <- function(bdd_grossesse, jobs_to_jobs_cat, t3_new_jobs, crb){
  # init mom occupation ----
  mom_occupation <- bdd_grossesse
  
  # prepare ident for merge
  mom_occupation$ident <- as.character(mom_occupation$ident)
  crb$ident <- as.character(crb$ident)
  jobs_to_jobs_cat <- jobs_to_jobs_cat %>% mutate_all(as.character)
  
  # add sample date ----
  t1_dates <- crb %>% 
    filter(period == "T1") %>% 
    select(ident, sample_date) %>% 
    rename(sample_date_t1 = sample_date)
  t3_dates <- crb %>% 
    filter(period == "T3") %>% 
    select(ident, sample_date) %>% 
    rename(sample_date_t3 = sample_date)
  
  mom_occupation <- left_join(mom_occupation, t1_dates, by = "ident") %>%
    left_join(t3_dates, by = "ident")
  
  # mom professional status ----
  mom_occupation <- mom_occupation %>%
    mutate(
      mom_professional_status = as.factor(mt1eaa1_q01),
      mom_professional_status = recode(mom_professional_status,
                                       "1" = "Unemployed not looking for work",
                                       "2" = "Unemployed looking for work",
                                       "3" = "Employed")
    )
  
  # create end_work_date ----
  mom_occupation <- mom_occupation %>%
    mutate(
      end_work_date = 
        case_when(
          mt3sad1_q02 == 0 ~ po_datelmp, # If never worked during preg, end of date = doc
          !is.na(mt3sad1_q02p2) ~ mt3sad1_q02p2, # End work date = other date when given
          !is.na(mt3sad1_q02p1) ~ mt3sad1_q02p1 # If no other date given, date start maternity leave
        ))
  
  # create mom_occupation ----
  
  ###############################################################################- 
  # Here is the code that produced the list opf jobs used for the manual
  # classification list_of_jobs <- bdd_grossesse$mt1eaa1_q01p11 %>%
  # str_to_lower(.) %>% remove_accents(.) %>% unique(.) %>% as.data.frame(.)
  # write_csv(list_of_jobs,
  # "analyses/phenols_descriptive_analysis/list_of_jobs.csv")
  ###############################################################################- 
  
  # * get full job description ----
  mom_occupation <- mom_occupation %>% 
    mutate(ident = as.character(ident))

  # * merge categories produced manually ----
  mom_occupation <- left_join(mom_occupation, 
                              jobs_to_jobs_cat, 
                              by = "ident") %>%
    rename(
      "mom_occupation1_t1" = "job_cat1",
      "mom_occupation2_t1" = "job_cat2",
      "mom_occupation3_t1" = "job_cat3"
    )
  
  # * manual corrections ----
  ###############################################################################- 
  # get by hand those that skipped the left join due to accents (this is not very
  # good but we went around this problematic the wrong way, I should have used
  # the ids to merge but now we have done the classification by hand in the excel
  # file it is too late) tab <- mom_occupation %>% filter(!is.na(job) &
  # is.na(mom_occupation3_t1)) %>% select(ident, job)
  ###############################################################################- 
  
  # 1 14496 PROF DES ÉCOLES
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 14496] <- "Teacher"
  # 2 21839 Éducatrice de jeunes enfants en multi-accueil 
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 21839] <- "Teacher"
  # 3 22284 Entraîneur de Gymnastique Rythmique + animatrice Baby Gym + secrétariat  
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 22284] <- "Teacher"
  # 4 19244 Archéologue (responsable de chantiers de fouille sur des bâtiments)      
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 19244] <- "Other"
  # 5 19490 "Psychologue clinicienne dans la fonctionn publique \r\nhospitalière
  # (secteur Établissement pour personnes âgées \r\~
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 19490] <- "Healthcare worker"
  # 6 26650 Éducatrice spécialisée, accompagnement social de familles,
  # primo-arrivantes pour la plupart
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 26650] <- "Sales or service worker"
  # 7 16546 Technicienne de laboratoire à l’Établissement français du sang.  
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 16546] <- "Other sector at risk"
  # 8 25337 "Manipulatrice électroradiologie médicale en salle de
  # \r\ncardiologie interventionnelle. Faisant fonction cadre.\r\n~
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 25337] <- "Healthcare worker"
  
  #two extra manual modification (see Sarah's email from 10/04/2019
  ###############################################################################- 
  # Matthieu, je viens de discuter avec Claire à l'instant, pour ne pas attendre
  # la mise à jour de la base, voila les deux volontaires impactées par ses
  # modifications : ident = 15741 --> la variable Q01P13 du questionnaire MT1EAA1
  # était manquant, sa valeur est : 'Ingénieur, cadre ou profession
  # intellectuelle sup" ident = 18086 --> elle n'a pas rempli de questionnaire
  # MT1EAA1, sa profession est sage-femme (j'ai pu remplir le questionnaire
  # MT1EAA1 partiellement grâce à des informations qu'elle n’aurait pas du
  # remplir dans le questionnaire MT3SAD1)
  ###############################################################################- 
  
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 15741] <- "Office worker"
  mom_occupation$mom_occupation3_t1[mom_occupation$ident == 18086] <- "Healthcare worker"
  
  # * add unemployed to occupation ----
  # using the professionnal status variable
  mom_occupation$mom_occupation3_t1 <- ifelse(!is.na(mom_occupation$mom_professional_status) &
                                                mom_occupation$mom_professional_status %in% 
                                                c("Unemployed not looking for work", 
                                                  "Unemployed looking for work"),
                                              "Unemployed",
                                              mom_occupation$mom_occupation3_t1)
  
  # Use questionnaire date, sample date and end of job date to see if person was
  # employed at time of sample
  mom_occupation <- mom_occupation %>%
    rename(
      job_qdate_t1 = mt1eaa1_date_creation,
      job_start_date_t1 = mt1eaa1_q01p5,
      job_end_date_t1 = mt1eaa1_q01p7,
      prev_job_start_date_t1 = mt1eaa1_q01p19,
      prev_job_end_date_t1 = mt1eaa1_q01p21
    ) %>%
    mutate(
      job_qdate_t1 = ymd(job_qdate_t1),
      job_start_date_t1 = ymd(job_start_date_t1),
      job_end_date_t1 = ymd(job_end_date_t1),
      prev_job_start_date = ymd(prev_job_start_date_t1),
      prev_job_end_date_t1 = ymd(prev_job_end_date_t1
      )
    )
  
  # * conditionnal correction ----
  ###############################################################################- 
  # Conditional correction of occupation
  # if sample date > questionnaire date then no change
  # if sample date < questionnaire date then
  # if date start job > sample date => look at previous job
  # count number of times this happened:
  # sum((mom_occupation$sample_date_t1 < mom_occupation$job_qdate_t1) & 
  #       (mom_occupation$job_start_date_t1 > mom_occupation$sample_date_t1), na.rm = TRUE)
  # N = 1
  # Here we check in which situation we are 
  # sum((mom_occupation$sample_date_t1 < mom_occupation$job_qdate_t1) & 
  #       (mom_occupation$job_start_date_t1 > mom_occupation$sample_date_t1) &
  #       (mom_occupation$prev_job_start_date_t1 < mom_occupation$sample_date_t1) &
  #       (mom_occupation$prev_job_end_date_t1 < mom_occupation$sample_date_t1), na.rm = TRUE)
  ###############################################################################- 
  
  # The woman ended her previous job before the sample and started between the
  # sample and questionnaire so she was unemployed at the time of sample
  mom_occupation$mom_occupation3_t1[
    (mom_occupation$sample_date_t1 < mom_occupation$job_qdate_t1) & 
      (mom_occupation$job_start_date_t1 > mom_occupation$sample_date_t1) &
      (mom_occupation$prev_job_start_date_t1 < mom_occupation$sample_date_t1) &
      (mom_occupation$prev_job_end_date_t1 < mom_occupation$sample_date_t1)
    ] <- "Unemployed"
  
  ###############################################################################-
  # if date start job < sample date
  # if date end job < sample date then unemployed
  # count number of times this happened:
  # sum((mom_occupation$sample_date_t1 < mom_occupation$job_qdate_t1) & 
  #       (mom_occupation$job_end_date_t1 < mom_occupation$sample_date_t1), na.rm = TRUE)
  # N = 33
  # mom_occupation$mom_professional_status[
  #   (mom_occupation$sample_date_t1 < mom_occupation$job_qdate_t1) &
  #     (mom_occupation$job_end_date_t1 < mom_occupation$sample_date_t1)
  # ]
  # => they are already all set to unemployed
  #
  # if sample date < date end job < questionnaire date then no change
  # if no end date then no change
  #
  # T3 and maternity leave
  # N changes since T1 table(bdd_grossesse$mt3sad1_q02p3) N=26
  ###############################################################################-
  
  # If stop working before sample date then unemployed
  mom_occupation$mom_occupation3_t1 <- ifelse(!is.na(mom_occupation$end_work_date) & 
                                                !is.na(mom_occupation$sample_date_t1) &
                                                mom_occupation$end_work_date < mom_occupation$sample_date_t1, 
                                              "Unemployed", mom_occupation$mom_occupation3_t1)
  
  # occupation at T3 ----
  # * init job at t3 ----
  # get new jobs
  mom_occupation <- left_join(mom_occupation, t3_new_jobs, by = "job") %>%
    rename(
      "new_job_t3" = "job_cat1"
    )
  # init at job at t3 = job at t1
  mom_occupation$mom_occupation3_t3 <- mom_occupation$mom_occupation3_t1
  
  ###############################################################################-
  # Correct if change since T3
  # List new jobs
  # t3_new_jobs <- bdd_grossesse$mt3sad1_q02p5[!is.na(bdd_grossesse$mt3sad1_q02p3 == "1") & 
  #                                              bdd_grossesse$mt3sad1_q02p3 == "1"]
  # t3_new_jobs <- t3_new_jobs %>%
  #   str_to_lower(.) %>%
  #   remove_accents(.) %>%
  #   unique(.) %>%
  #   as.data.frame(.)
  # write_csv(t3_new_jobs, "t3_new_jobs.csv")
  ###############################################################################-
  
  # mom_occupation <- mom_occupation %>%
  #   rename(job_t3 = mt3sad1_q02p5) %>%
  #   mutate(job_t3 = str_to_lower(job_t3),
  #          job_t3 = remove_accents(job_t3))
  
  ###############################################################################-
  # tab <- bdd_grossesse %>%
  #   select(ident, mt3sad1_q02p5) %>%
  #   filter(!is.na(mt3sad1_q02p5) & 
  #            is.na(questionnaire$mom_occupation3_t3))
  ###############################################################################-
  
  # * manual correction ----
  # 24255 Assistante maternelle au chomage
  mom_occupation$mom_occupation3_t3[mom_occupation$ident == 24255] <- "Unemployed"
  
  # * conditional correction ----
  # if new job since t1 then replace by new job 
  mom_occupation <- mom_occupation %>%
    mutate(
      mom_occupation3_t3 = case_when(
        !is.na(sample_date_t3) & !is.na(end_work_date) & end_work_date < sample_date_t3 ~ "Unemployed",
        !is.na(mt3sad1_q02p3) & mt3sad1_q02p3 == "1" ~ new_job_t3,
        TRUE ~ mom_occupation3_t3
      )
    )
  
  # Final occupation at T1 ----
  mom_occupation$mom_occupation_t1 <- mom_occupation$mom_occupation3_t1
  mom_occupation$mom_occupation_t1 <- ifelse(!is.na(mom_occupation$mom_occupation3_t1) &
                                               mom_occupation$mom_occupation3_t1 %in% 
                                               c("Office worker", "Other", "Sales or service worker", 
                                                 "Teacher", "Other sector at risk"),
                                             "Other",
                                             mom_occupation$mom_occupation_t1)
  
  # Final occupation at T3 ----
  mom_occupation$mom_occupation_t3 <- mom_occupation$mom_occupation3_t3
  mom_occupation$mom_occupation_t3 <- ifelse(!is.na(mom_occupation$mom_occupation3_t3) &
                                               mom_occupation$mom_occupation3_t3 %in% 
                                               c("Office worker", "Other", "Sales or service worker", 
                                                 "Teacher", "Other sector at risk"),
                                             "Other",
                                             mom_occupation$mom_occupation_t3)
  
  # keep occupation vars
  mom_occupation <- select(mom_occupation, ident, contains("occupation"))
  
  # wide to long ----
  occupation_final <- period_wide_to_long(mom_occupation)
  
  return(occupation_final)
}


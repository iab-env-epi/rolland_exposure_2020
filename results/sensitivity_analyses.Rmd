---
title: "Phenols descriptive"
subtitle: "Sensitivity analysis"
author: "Matthieu Rolland"
date: "`r Sys.Date()`"
output: 
  html_document:
    theme: cerulean
    toc: true
    number_sections: true
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE, warning = FALSE, message = FALSE)
```

```{r}
# for imputed model N's
loadd(phenols_data)
```

```{r}
mother_imputed_models <- readd(mother_models) # to gain time should import just data
infant_imputed_models <- readd(infant_models)
```

```{r}
# prepare imputed models to serve as reference
# mother
# init model output table
mother_imputed_tab <- data.frame(compound = character(),
                                 term = character(),
                                 est_i = numeric(),
                                 low_i = numeric(),
                                 high_i = numeric(),
                                 p_i = numeric())

for(i in 1:length(mother_imputed_models)){
  comp <- mother_imputed_models[[i]]$compound
  model_output <- mother_imputed_models[[i]]$summary %>%
    filter(!str_detect(term, "gauss|Log|Intercept")) %>%
    rename(est_i = estimate,
           p_i = p.value) %>%
    mutate(low_i = est_i - 1.96 * std.error,
           high_i = est_i + 1.96 * std.error,
           compound = mother_imputed_models[[i]]$compound) %>%
    dplyr::select(compound, term, est_i, low_i, high_i, p_i) %>%
    mutate_at(3:ncol(.), ~round(., 3))

  # save
  mother_imputed_tab <- bind_rows(mother_imputed_tab, model_output)
}

# infant
# init model output table
infant_imputed_tab <- data.frame(compound = character(),
                                 term = character(),
                                 est_i = numeric(),
                                 low_i = numeric(),
                                 high_i = numeric(),
                                 p_i = numeric())

for(i in 1:length(infant_imputed_models)){
  comp <- infant_imputed_models[[i]]$compound
  model_output <- infant_imputed_models[[i]]$summary %>%
    filter(!str_detect(term, "gauss|Log|Intercept")) %>%
    rename(est_i = estimate,
           p_i = p.value) %>%
    mutate(low_i = est_i - 1.96 * std.error,
           high_i = est_i + 1.96 * std.error,
           compound = infant_imputed_models[[i]]$compound) %>%
    dplyr::select(compound, term, est_i, low_i, high_i, p_i) %>%
    mutate_at(3:ncol(.), ~round(., 3))

  # save
  infant_imputed_tab <- bind_rows(infant_imputed_tab, model_output)
}


```

# Complete models 

## Mother complete


```{r}
# load model results
mother_complete_models <- readd(mother_models_complete)

# N cases total
n_total <- sum(phenols_data$period %in% c("T1", "T3"))

# init model output table
complete_tab <- data.frame(compound = character(),
                         N = character(),
                         term = character(),
                         est_c = numeric(),
                         low_c = numeric(),
                         high_c = numeric(),
                         p_c = numeric())

for(i in 1:length(mother_complete_models)){
  comp <- mother_complete_models[[i]]$compound
  n <- summary(mother_complete_models[[i]])$n
  # complete model for ith compound
  model_output_complete <- broom::tidy(mother_complete_models[[i]]) %>%
    filter(!str_detect(term, "gauss|Log|Intercept") &
             !is.na(estimate)) %>%
    mutate(compound = comp,
           N = str_c(n, " / ", n_total)) %>%
    dplyr::select(compound, N, term, estimate, conf.low, conf.high, p.value) %>%
    rename(est_c = estimate, 
           low_c = conf.low, 
           high_c = conf.high, 
           p_c = p.value) %>%
    mutate_at(4:ncol(.), ~round(., 3))
  
  # save
  complete_tab <- bind_rows(complete_tab, model_output_complete)
}

# combine complete models with imputed models
all_models <- left_join(complete_tab, mother_imputed_tab, by = c("compound", "term"))

all_models %>%
  mutate(
    est_c = cell_spec(est_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    low_c = cell_spec(low_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    high_c = cell_spec(high_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    p_c = cell_spec(p_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    est_i = cell_spec(est_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    low_i = cell_spec(low_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    high_i = cell_spec(high_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    p_i = cell_spec(p_i, bold = ifelse(p_i < 0.05, TRUE, FALSE))
  ) %>%
  kable(format = "html", escape = F, caption = "complete models") %>%
  kable_styling("condensed", full_width = F) %>%
  pack_rows("Bisphenol A", 1, 19) %>%
  pack_rows("Bisphenol S", 20, 38) %>%
  pack_rows("Methylparaben", 39, 53) %>%
  pack_rows("Ethylparaben", 54, 68) %>%
  pack_rows("Propylparaben", 69, 83) %>%
  pack_rows("Butylparaben", 84, 98) %>%
  pack_rows("Triclosan", 99, 115) %>%
  pack_rows("Benzophenone-3", 116, 130) %>%
  add_header_above(c(" " = 3, "Complete" = 4, "Imputed" = 4),
                   include_empty = T, bold = TRUE) %>%
  column_spec(8:11, background = "aliceblue")

```

## Infant complete

N total = `r sum(phenols_data$period %in% c("M2", "Y1"))`

```{r}
infant_complete_models <- readd(infant_models_complete)

n_total <- sum(phenols_data$period %in% c("M2", "Y1"))

# init model output table
complete_tab <- data.frame(compound = character(),
                         N = character(),
                         term = character(),
                         est_c = numeric(),
                         low_c = numeric(),
                         high_c = numeric(),
                         p_c = numeric())

for(i in 1:length(infant_complete_models)){
  comp <- infant_complete_models[[i]]$compound
  n <- summary(infant_complete_models[[i]])$n
  # complete model for ith compound
  model_output_complete <- broom::tidy(infant_complete_models[[i]]) %>%
    filter(!str_detect(term, "gauss|Log|Intercept") &
             !is.na(estimate)) %>%
    mutate(compound = comp,
           N = str_c(n, " / ", n_total)) %>%
    dplyr::select(compound, N, term, estimate, conf.low, conf.high, p.value) %>%
    rename(est_c = estimate, 
           low_c = conf.low, 
           high_c = conf.high, 
           p_c = p.value) %>%
    mutate_at(4:ncol(.), ~round(., 3))
  
  # save
  complete_tab <- bind_rows(complete_tab, model_output_complete)
}

# combine complete models with imputed models
all_models <- left_join(complete_tab, infant_imputed_tab, by = c("compound", "term"))


all_models %>%
  mutate(
    est_c = cell_spec(est_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    low_c = cell_spec(low_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    high_c = cell_spec(high_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    p_c = cell_spec(p_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    est_i = cell_spec(est_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    low_i = cell_spec(low_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    high_i = cell_spec(high_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    p_i = cell_spec(p_i, bold = ifelse(p_i < 0.05, TRUE, FALSE))
  ) %>%
  kable(format = "html", escape = F, caption = "complete models") %>%
  kable_styling("condensed", full_width = F) %>%
  pack_rows("Bisphenol A", 1, 18) %>%
  pack_rows("Bisphenol S", 19, 36) %>%
  pack_rows("Methylparaben", 37, 52) %>%
  pack_rows("Ethylparaben", 53, 68) %>%
  pack_rows("Propylparaben", 69, 84) %>%
  pack_rows("Butylparaben", 85, 100) %>%
  pack_rows("Triclosan", 101, 116) %>%
  pack_rows("Benzophenone-3", 117, 132) %>%
  add_header_above(c(" " = 3, "Complete" = 4, "Imputed" = 4),
                   include_empty = T, bold = TRUE) %>%
  column_spec(8:11, background = "aliceblue")
```

# Methylparaben conjugated

```{r}
conj_models <- readd(conj_models)


mepa_conj <- conj_models[[1]]$summary %>%
  filter(!str_detect(term, "gauss|Log|Intercept")) %>%
  mutate(conf.low = estimate - 1.96 * std.error,
         conf.high = estimate + 1.96 * std.error,
         compound = conj_models[[1]]$compound) %>%
  dplyr::select(compound, term, estimate, conf.low, conf.high, p.value) %>%
  rename(
    est_c = estimate, 
    low_c = conf.low, 
    high_c = conf.high, 
    p_c = p.value
  )

mepa_total <- infant_imputed_models[[1]]$summary %>%
  filter(!str_detect(term, "gauss|Log|Intercept")) %>%
  mutate(conf.low = estimate - 1.96 * std.error,
         conf.high = estimate + 1.96 * std.error,
         compound = infant_imputed_models[[1]]$compound) %>%
  dplyr::select(compound, term, estimate, conf.low, conf.high, p.value)%>%
  rename(
    est_t = estimate, 
    low_t = conf.low, 
    high_t = conf.high, 
    p_t = p.value
  )

# combine both
mepa_models <- left_join(mepa_conj, mepa_total, 
                         by = c("term")) %>%
  select(-contains("comp"))

mepa_models <- mepa_models %>%
  mutate(
    est_c = round(est_c, 2),
    low_c = round(low_c, 2),
    high_c = round(high_c, 2),
    p_c = round(p_c, 3),
    est_t = round(est_t, 2),
    low_t = round(low_t, 2),
    high_t = round(high_t, 2),
    p_t = round(p_t, 3)
  )

colnames(mepa_models) <- c("   ", "est_c", "low_c", "high_c", "p_c", "est_t", "low_t", "high_t", "p_t")

mepa_models %>%
  mutate(
    est_c = cell_spec(est_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    low_c = cell_spec(low_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    high_c = cell_spec(high_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    p_c = cell_spec(p_c, bold = ifelse(p_c < 0.05, TRUE, FALSE)),
    est_t = cell_spec(est_t, bold = ifelse(p_t < 0.05, TRUE, FALSE)),
    low_t = cell_spec(low_t, bold = ifelse(p_t < 0.05, TRUE, FALSE)),
    high_t = cell_spec(high_t, bold = ifelse(p_t < 0.05, TRUE, FALSE)),
    p_t = cell_spec(p_t, bold = ifelse(p_t < 0.05, TRUE, FALSE))
  ) %>%
  kable(format = "html", escape = F, caption = "complete models") %>%
  kable_styling("condensed", full_width = F) %>%
  add_header_above(c(" " = 1, "Conjugated" = 4, "Total" = 4),
                   include_empty = T, bold = TRUE) %>%
  column_spec(6:9, background = "aliceblue")

```

# Logistic regressions

## Mother

```{r}
mother_logistic_models <- readd(mother_models_logistic)

```

```{r}
# BPS
tab_logistic_bps <- summary(pool(mother_logistic_models[[1]])) %>%
  filter(!str_detect(term, "gauss|Log|Intercept")) %>%
  mutate(compound = mother_logistic_models[[1]]$compound,
         low = estimate - 1.96*std.error,
         high = estimate + 1.96*std.error) %>%
  select(compound, term, estimate, low, high, p.value) %>%
  rename(est_l = estimate,
         low_l = low,
         high_l = high,
         p_l = p.value) %>%
  mutate_at(3:ncol(.), ~round(., 3))

tab_imputed_bps <- filter(mother_imputed_tab, compound == "BPS_total") %>%
  mutate(compound = str_remove(compound, "_total")) # safeguard against a change in compound order

tab_bps <- left_join(tab_logistic_bps, tab_imputed_bps, by = c("compound","term")) 

# BUPA
tab_logistic_bupa <- summary(pool(mother_logistic_models[[2]])) %>%
  filter(!str_detect(term, "gauss|Log|Intercept")) %>%
  mutate(compound = mother_logistic_models[[2]]$compound,
         low = estimate - 1.96*std.error,
         high = estimate + 1.96*std.error) %>%
  select(compound, term, estimate, low, high, p.value) %>%
  rename(est_l = estimate,
         low_l = low,
         high_l = high,
         p_l = p.value) %>%
  mutate_at(3:ncol(.), ~round(., 3))

tab_imputed_bupa <- filter(mother_imputed_tab, compound == "BUPA_total") %>%
  mutate(compound = str_remove(compound, "_total"))

tab_bupa <- left_join(tab_logistic_bupa, tab_imputed_bupa, by = c("compound","term"))

logistic_models <- bind_rows(tab_bps, tab_bupa)

# output tab
logistic_models %>%
  mutate(
    est_l = cell_spec(est_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    low_l = cell_spec(low_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    high_l = cell_spec(high_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    p_l = cell_spec(p_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    est_i = cell_spec(est_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    low_i = cell_spec(low_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    high_i = cell_spec(high_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    p_i = cell_spec(p_i, bold = ifelse(p_i < 0.05, TRUE, FALSE))
  ) %>%
  kable(format = "html", escape = F, caption = "complete models") %>%
  kable_styling("condensed", full_width = F) %>%
  add_header_above(c(" " = 2, "Logistic" = 4, "Imputed" = 4),
                   include_empty = T, bold = TRUE) %>%
  column_spec(7:10, background = "aliceblue") %>%
  pack_rows("Bisphenol S", 1, 19) %>%
  pack_rows("Butylparaben", 20, 34)
```

## Infant

```{r}
infant_logistic_models <- readd(infant_models_logistic)

```

```{r}
# BPS
tab_logistic_bps <- summary(pool(infant_logistic_models[[1]])) %>%
  filter(!str_detect(term, "gauss|Log|Intercept")) %>%
  mutate(compound = infant_logistic_models[[1]]$compound,
         low = estimate - 1.96*std.error,
         high = estimate + 1.96*std.error) %>%
  select(compound, term, estimate, low, high, p.value) %>%
  rename(est_l = estimate,
         low_l = low,
         high_l = high,
         p_l = p.value) %>%
  mutate_at(3:ncol(.), ~round(., 3))

tab_imputed_bps <- filter(infant_imputed_tab, compound == "BPS_total") %>%
  mutate(compound = str_remove(compound, "_total")) # safeguard against a change in compound order

tab_bps <- left_join(tab_logistic_bps, tab_imputed_bps, by = c("compound","term")) 

# BUPA
tab_logistic_bupa <- summary(pool(infant_logistic_models[[2]])) %>%
  filter(!str_detect(term, "gauss|Log|Intercept")) %>%
  mutate(compound = infant_logistic_models[[2]]$compound,
         low = estimate - 1.96*std.error,
         high = estimate + 1.96*std.error) %>%
  select(compound, term, estimate, low, high, p.value) %>%
  rename(est_l = estimate,
         low_l = low,
         high_l = high,
         p_l = p.value) %>%
  mutate_at(3:ncol(.), ~round(., 3))

tab_imputed_bupa <- filter(infant_imputed_tab, compound == "BUPA_total") %>%
  mutate(compound = str_remove(compound, "_total"))

tab_bupa <- left_join(tab_logistic_bupa, tab_imputed_bupa, by = c("compound","term"))

logistic_models <- bind_rows(tab_bps, tab_bupa)

logistic_models %>%
  mutate(
    est_l = cell_spec(est_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    low_l = cell_spec(low_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    high_l = cell_spec(high_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    p_l = cell_spec(p_l, bold = ifelse(p_l < 0.05, TRUE, FALSE)),
    est_i = cell_spec(est_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    low_i = cell_spec(low_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    high_i = cell_spec(high_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    p_i = cell_spec(p_i, bold = ifelse(p_i < 0.05, TRUE, FALSE))
  ) %>%
  kable(format = "html", escape = F, caption = "complete models") %>%
  kable_styling("condensed", full_width = F) %>%
  add_header_above(c(" " = 2, "Logistic" = 4, "Imputed" = 4),
                   include_empty = T, bold = TRUE) %>%
  column_spec(7:10, background = "aliceblue") %>%
  pack_rows("Bisphenol S", 1, 18) %>%
  pack_rows("Butylparaben", 19, 34)
```


# Models without smoke, canned food and occupation

```{r}
non_sparse_models <- readd(non_sparse_models)
```

```{r}
all_models <- data.frame(compound = character(), 
                         reason = character(), 
                         term = character(), 
                         est_s = numeric(), 
                         low_s = numeric(), 
                         high_s = numeric(), 
                         p_s = numeric())

for(i in 1:length(non_sparse_models)){
  model_output <- non_sparse_models[[i]]$summary %>%
    filter(!str_detect(term, "gauss|Log|Intercept")) %>%
    dplyr::select(-statistic, -df) %>%
    mutate(conf.low = estimate - 1.96 * std.error,
           conf.high = estimate + 1.96 * std.error,
           compound = non_sparse_models[[i]]$compound,
           reason = non_sparse_models[[i]]$reason) %>%
    rename(est_s = estimate,
           low_s = conf.low,
           high_s = conf.high,
           p_s = p.value) %>%
    select(compound, reason, term, est_s, low_s, high_s, p_s)
  all_models <- bind_rows(all_models, model_output)
}

all_models <- all_models %>%
  mutate_at(4:ncol(.), ~round(., 3))

sparse_models <- left_join(all_models, mother_imputed_tab, by = c("compound", "term"))

sparse_models %>%
  mutate(
    est_s = cell_spec(est_s, bold = ifelse(p_s < 0.05, TRUE, FALSE)),
    low_s = cell_spec(low_s, bold = ifelse(p_s < 0.05, TRUE, FALSE)),
    high_s = cell_spec(high_s, bold = ifelse(p_s < 0.05, TRUE, FALSE)),
    p_s = cell_spec(p_s, bold = ifelse(p_s < 0.05, TRUE, FALSE)),
    est_i = cell_spec(est_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    low_i = cell_spec(low_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    high_i = cell_spec(high_i, bold = ifelse(p_i < 0.05, TRUE, FALSE)),
    p_i = cell_spec(p_i, bold = ifelse(p_i < 0.05, TRUE, FALSE))
  ) %>%
  kable(format = "html", escape = F, caption = "non sparse model") %>%
  kable_styling("condensed", full_width = F) %>%
  add_header_above(c(" " = 3, "Non sparse" = 4, "Imputed" = 4),
                   include_empty = T, bold = TRUE) %>%
  column_spec(8:11, background = "aliceblue") %>%
  pack_rows("No Cashier - Bisphenol A", 1, 17) %>%
  pack_rows("No Cashier - Bisphenol S", 18, 34) %>%
  pack_rows("No Healthcare worker - Triclosan", 35, 49) %>%
  pack_rows("No Canned food - Bisphenol A", 50, 67) %>%
  pack_rows("No Canned food - Bisphenol S", 68, 85) %>%
  pack_rows("No Smoke - Bisphenol A", 86, 103) %>%
  pack_rows("No Smoke - Bisphenol S", 104, 121) %>%
  pack_rows("No Occupation - Canned food - Smoke - Bisphenol A", 122, 136) %>%
  pack_rows("No Occupation - Canned food - Smoke - Bisphenol S", 137, 151) %>%
  pack_rows("No Occupation - Canned food - Smoke -  Triclosan", 152, 166) 

```

